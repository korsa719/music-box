import React from 'react';
import Albums from './Albums';
import Favorites from './Favorites';
import Admin from './Admin';
import LogIn from './LogIn';
import AlbumsTracks from './AlbumsTracks';
import Header from './Header';
import {loadAlbums} from '../actions/albumsActions';
import {Route,Link,Switch} from 'react-router-dom';
import {connect} from 'react-redux';
import ReactAudioPlayer from 'react-audio-player';



class App extends React.Component{

  componentDidMount () {
    if(this.props.albums.length==0){
      this.props.loadData();
    }
}


    render(){
			console.log(this.props.albums);
			const res = this.props.albums;
			const WrappedAlbum = function(props) {
			            return <LogIn posts={res} />;
			        };
        return (
            <div>
                <header>
                    <Header />
                </header>
                <Switch>
                    <Route exact path="/" component={Albums}/>
                    <Route exact path="/albums/:id/tracks" component={AlbumsTracks }/>
                    <Route exact path="/favorites" component={Favorites}/>
                    <Route  path="/admin" component={Admin}/>
                    <Route exact path="/login" component={WrappedAlbum}/>
                </Switch>
            </div>
        )
    }
}
const  mapDispatchToProps = dispatch=> {
  return{
    loadData: () => dispatch(loadAlbums())
    }
  }

const mapStateToProps = state =>{
    return {
      albums: state.albums
    }
}

export default  connect(mapStateToProps, mapDispatchToProps)(App);
