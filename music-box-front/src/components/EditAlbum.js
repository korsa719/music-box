import React, {Component} from 'react';
import {connect} from 'react-redux';
import {loadAlbums} from '../actions/albumsActions';

class EditAlbum extends Component {
  constructor(props){
  	super(props);
  	this.state = {
      title:'',
      cover:''
    };
  }

    handleSubmit=(event)=>{
      event.preventDefault();
      fetch('http://localhost:8080/albums/'+this.props.match.params.id,
      {
          method: 'PUT',
          headers: {
            'Accept': 'application/json',
                   'Content-Type': 'application/json'
               },
          body: JSON.stringify({
                    title:this.state.title === '' ? this.props.album.title : this.state.title,
                    cover:this.state.cover === '' ? this.props.album.cover : this.state.cover,
                  })

      }).then(()=>this.props.loadData());


    }

    handlerTitle (event){
      console.log('log',event.target.value);
    this.setState({title:event.target.value})

    }
    handlerCover (event){
      console.log('log',event.target.value);
    this.setState({cover:event.target.value})

    }

    render() {

        const album = this.props.album
        return album ? (
          <form onSubmit={this.handleSubmit}  >
               Title:
           <input type="text" name='title' id='title' onChange={this.handlerTitle.bind(this)} placeholder={this.props.album.title} />
               Cover:
           <input type='text' name='cover' id='cover' onChange={this.handlerCover.bind(this )} placeholder={this.props.album.cover}   />
           <button>Submit</button>
         </form>
       ) : 'null'
    }
}

const mapStateToProps = (state, ownParams) =>{
    return {
      album: state.albums.find(alb=> alb.id == ownParams.match.params.id)
    }
}
const  mapDispatchToProps = dispatch=> {
  return{
    toogleEditAlbum: () => dispatch({type:'TOGGLE_EDIT_ALBUM'}),
      loadData: () => dispatch(loadAlbums())
  }
}


export default  connect(mapStateToProps, mapDispatchToProps)(EditAlbum);
