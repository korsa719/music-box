import React,{Component} from 'react';

import {connect} from 'react-redux';
import {loadAlbums} from '../actions/albumsActions';

 class AddTrack extends Component {
  constructor(props){
  	super(props);
  	this.state = {
      trackName:'',
      trackUpload:'',
      editTrack:null,
      editTrackName:'',
      editTrackUpload:''
    };
  }
  handleSubmitUpload(event) {
      event.preventDefault();
      const form = event.target;
      const data = new FormData(form);
      fetch('http://localhost:8080/post', {
          method: 'POST',
          body: data
      })
        .then(res => res.json())
        .then(data => this.setState({trackUpload:data.trackUpload}))

  }
  //addTrack form
  handleSubmit=(event)=>{
    event.preventDefault();
    fetch('http://localhost:8080/track/',
    {
        method: 'POST',
        headers: {
                 'Content-Type': 'application/json',
             },
        body:JSON.stringify({
                  id:this.props.match.params.id,
                  trackName: this.state.trackName,
                  trackUpload: this.state.trackUpload
                })

    }).then(()=>this.props.loadData());

  }
  //editTrack form
  handleSubmitEditForm=(event)=>{
    event.preventDefault();
    if(this.state.editTrackName!== null && this.state.editTrackUpload !==null ){
    fetch('http://localhost:8080/track/tracks/'+this.state.editTrack,
    {
        method: 'PUT',
        headers: {
                 'Content-Type': 'application/json',
             },
        body:JSON.stringify({
                  id:this.state.editTrack,
                  trackName: this.state.editTrackName,
                  trackUpload: this.state.editTrackUpload
                })

    }).then(()=>this.props.loadData());
  }
  }
  //deleteTrack button
  deleteTrack= id =>{
    fetch('http://localhost:8080/track/tracks/'+id, {
        method: 'DELETE',
    })
    .then(()=>this.props.loadData());

  }
  //editTrack button  show form to this track  toChange
    editTrack= id =>{
      this.setState({editTrack:id})
    }
//addTrack field trackTitle save value to state
  handlerTitle (event){
    this.setState({trackName:event.target.value})
  }

  //editTrack field name save value to state editTrackName
  editTitle (event){
    this.setState({editTrackName:event.target.value})
  }
  //editTrack field trackUpload save value to state editTrackUpload
  editTrackUpload (event){
    this.setState({editTrackUpload:event.target.value})
  }

    render() {
        const tracks = this.props.album ? this.props.album.tracks : []

        return (
          <div>
            <form  encType="multipart/form-data" onSubmit={this.handleSubmitUpload.bind(this)}>
                Track to upload:
                <input type="file" name="file" />
                <input type="submit" value="Upload" />
            </form>
                <form onSubmit={this.handleSubmit}  >
                     TrackTitle:
                 <input type="text" id='trackName' onChange={this.handlerTitle.bind(this)} placeholder='trackName' />
                 <button>Submit</button>
               </form>
                {
                  tracks.map((track,index)=>
                  <p key={index}>
                    Name: <a>{track.trackName}</a>
                    URL: <a>{track.trackUpload}</a>
                    <button onClick={()=>this.deleteTrack(track.id)}>x</button>
                    <button onClick={()=>this.editTrack(track.id)}>edit</button>
                    {this.state.editTrack === track.id ? <form onSubmit={this.handleSubmitEditForm}>
                      TrackTitle:
                  <input type="text" id='trackName' onChange={this.editTitle.bind(this)} placeholder={track.trackName} />
                      Track to upload:
                    <input type="file" name="file" />
                   <button>Submit</button>
                 </form>:''}
                  </p>)
                }
         </div>
        );
    }
}
const mapStateToProps = (state,ownparams) =>{
    return {
      albums: state.albums,
      album: state.albums.find(album=> album.id == ownparams.match.params.id)
    }
}
const  mapDispatchToProps = dispatch=> {
  return{
    loadData: () => dispatch(loadAlbums())
  }
}


export default  connect(mapStateToProps, mapDispatchToProps)(AddTrack);
