import React,{Component} from 'react';
import {connect} from 'react-redux';

class Favorites extends Component {

  componentDidMount () {

    fetch('http://localhost:8080/track/tracks/1/like',{
      method:'GET'})
      .then(res => res.json())
      .then(data =>this.props.onLoadTrack(data));

}

play=trackUpload=>{
  this.props.playURL(trackUpload);
}
    render() {

        return (
          <div className="gallery">
            {this.props.favoritesTracks.map((track,index)=>
              <p key={index}>{track.trackName}
                <button onClick={()=>this.play(track.trackUpload)}>play</button>
              </p>)}
          </div>
        );
    }
}

const  mapDispatchToProps = dispatch=> {
  return{
    onLoadTrack:track=>{
      dispatch({type:'TRACKS_LOADED',payload:track});
    },
    playURL:(trackURL)=>{
      dispatch({type:'PLAYING_TRACK',payload:trackURL});
    }
  }
}
const mapStateToProps = state =>{
  return{
      playingTrack:state.playingTrack,
    favoritesTracks: state.favoritesTracks
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(Favorites)
