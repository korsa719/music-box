import React,{Component} from 'react';
import EditAlbum from './EditAlbum';
import {connect} from 'react-redux';
import {loadAlbums} from '../actions/albumsActions';
import {Route,Link,Switch} from 'react-router-dom';


class Edit extends Component {
deleteAlbum=id=>{
  fetch('http://localhost:8080/albums/'+id, {
      method: 'DELETE',
  })
  .then(()=>this.props.loadData());
}

deleteAlbumCover=id=>{
  fetch('http://localhost:8080/albums/'+id+'/cover', {
      method: 'DELETE',
  })
  .then(()=>this.props.loadData());
}


editAlbum = album =>{
  this.props.toogleEditAlbum();

}
    render() {
      const list = this.props.albums.map(album =>
        <div key={album.id}>
          {album.title}
          <button onClick={()=>this.deleteAlbum(album.id)}>x</button>
          <Link to={'/admin/albums/edit/'+album.id+'/album'}  ><button>edit</button></Link>
          <button onClick={()=>this.deleteAlbumCover(album.id)}>deleteAlbumCover</button>
          <Link to={'/admin/track/'+album.id}  ><button>addTrack</button></Link>
        </div>
      )

        return (
          <div>
          {list}
        </div>

        );
    }
}
const mapStateToProps = state =>{
    return {
      albums: state.albums,
      isEditAlbum: state.isEditAlbum
    }
}
const  mapDispatchToProps = dispatch=> {
  return{
    loadData: () => dispatch(loadAlbums()),
    toogleEditAlbum: () => dispatch({type:'TOGGLE_EDIT_ALBUM'})
  }
}


export default  connect(mapStateToProps, mapDispatchToProps)(Edit);
