import React,{Component} from 'react';
import AddAlbums from './AddAlbums';
import UploadFile from './UploadFile';
import Edit from './Edit';
import EditAlbum from './EditAlbum';
import AddTrack from './AddTrack';
import {Route,Link,Switch} from 'react-router-dom';
import '../App.css';
export default class Admin extends Component {
  render(){

    return (
      <div>
        <header id="admin_menu">
            <Link  className="admin_menu_nav" to="/admin/albums">AddAlbums</Link>
            <Link className="admin_menu_nav" to="/admin/upload">UploadFile</Link>
            <Link  className="admin_menu_nav" to="/admin/albums/edit">Edit</Link>
        </header>
        <Switch>
            <Route exact path="/admin/albums" component={AddAlbums} />
            <Route exact path="/admin/upload" component={UploadFile} />
            <Route exact path="/admin/albums/edit" component={Edit} />
            <Route path="/admin/albums/edit/:id" component = {EditAlbum} />
            <Route path="/admin/track/:id" component={AddTrack} />
        </Switch>
      </div>
    );
  }
}
