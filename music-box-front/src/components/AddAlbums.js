import React,{Component} from 'react';
import {connect} from 'react-redux';
import {loadAlbums} from '../actions/albumsActions';

 class AddAlbums extends Component {
   constructor(props){
   	super(props);
   	this.state = {
       title:'',
       cover:''
     };
   }
   handleSubmitUpload(event) {
       event.preventDefault();
       const form = event.target;
       const data = new FormData(form);
       fetch('http://localhost:8080/post', {
           method: 'POST',
           body: data
       })
         .then(res => res.json())
         .then(data => this.setState({cover:data.trackUpload}))



   }
     handleSubmit(event) {
       event.preventDefault();

         fetch('http://localhost:8080/albums', {
             method: 'POST',
             headers: {
                      'Content-Type': 'application/json',
                  },
             body: JSON.stringify({
                       title: this.state.title,
                       cover: this.state.cover
                     })
         }).then(()=>this.props.loadData());

         console.log('title',this.state.title);
         console.log('cover',this.state.cover);

     }
     handlerTitle (event){
       this.setState({title:event.target.value})
     }
    render() {
        return (
          <div>
          <form  encType="multipart/form-data" onSubmit={this.handleSubmitUpload.bind(this)}>
              Cover to upload:
              <input type="file" name="file" />
              <input type="submit" value="Upload" />
          </form>

        <form  onSubmit={this.handleSubmit.bind(this)} >
             Title:
         <input type="text" name='title' id='title'  onChange={this.handlerTitle.bind(this)}  placeholder='Album title'  />
         <button>Submit</button>
        </form>
      </div>

        );
    }
}
const  mapDispatchToProps = dispatch=> {
  return{
    loadData: () => dispatch(loadAlbums())
  }
}
const mapStateToProps = state =>{
    return {
      albums: state.albums
    }
}

export default  connect(mapStateToProps, mapDispatchToProps)(AddAlbums);
