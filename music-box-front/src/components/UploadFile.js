import React,{Component} from 'react';
import {connect} from 'react-redux';


 export default class UploadFile extends Component {
   constructor(props){
   	super(props);
   	this.state = {
      status:''
    };
   }
  handleSubmit(event) {
      event.preventDefault();
      const form = event.target;
      const data = new FormData(form);
      fetch('http://localhost:8080/post', {
          method: 'POST',
          body: data
      })
        .then(res => res.json())
        .then(data => this.setState({status:data.trackUpload}))

  }
    render() {
      const uploadList = this.props.uploadList.map(file=> <h3>{file}</h3>)
      console.log('state',  this.state.status);
        return (
          <div>
          <form  encType="multipart/form-data" onSubmit={this.handleSubmit.bind(this)}>
              File to upload:
              <input type="file" name="file" />
              <a>{this.state.status!=''? 'Ok     ':''}</a>
              <input type="submit" value="Upload" />
          </form>

        </div>
        );
    }
}
