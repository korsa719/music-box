import React, { Component } from 'react';
import {Route, Link, Switch } from 'react-router-dom'
import ReactAudioPlayer from 'react-audio-player';
import {connect} from 'react-redux';

import '../Header.css';


class Header extends Component  {


    render() {
        return (
            <div  id="header">
                <div className="topPanel">
                    <div className="topPanel-panel">
                        <div className="topPanel-left">
                            <div className="topPanelNav">

                                    <Link to="/"><span className="topPanelNavButton-text">Albums</span></Link>
                                    <Link to="/favorites"><span className="topPanelNavButton-text">Favorites</span></Link>
                                    <Link to="/admin"><span className="topPanelNavButton-text">Admin</span></Link>

                            </div>
                        </div>
                       {/* <div className="topPanel-center">
                            <a className="topPanelNavButton topPanelNavButton_selected">
                                <Link to="/add/album"><span className="topPanelNavButton-text">Add Album </span>  </Link>
                                <Link to="/favorite"><span className="topPanelNavButton-text">Favorites </span>  </Link>
                            </a>

                        </div>*/}
                        <div className="topPanel-right">
                           <div className="topPanel-rightInner">
                               <div className="topPanelVolume topPanelVolume_normal">
                                            <span className="topPanelVolume-icon"> </span>
                                               {/* <span className="topPanelVolume-slider">
                                                    <span className="topPanelVolume-handle" onMouseDown={this.handleMouseDown} style={{left:50+'%',}}> </span>

                                                    <span className="topPanelVolume-size"> </span>
                                                    <span className="topPanelVolume-volume"  style={{width:50+'%',}}> </span>
                                                </span>*/}
                     </div>
                     <ReactAudioPlayer
                       src={this.props.playingTrack}
                       autoPlay
                       controls
                     />
                    <div className="topPanelUserBar">
                       <div className="topPanelUserBar-signIn">
                             <Link to="/login"><span className="topPanelUserBar-signIn">Log In</span></Link>
                       </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
    }
}
const  mapDispatchToProps = dispatch=> {
  return{

    }
  }

const mapStateToProps = state =>{
    return {
      playingTrack:state.playingTrack
    }
}

export default  connect(mapStateToProps, mapDispatchToProps)(Header);
