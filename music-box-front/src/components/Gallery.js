import React,{Component} from 'react';
import {connect} from 'react-redux';


 class Gallery extends Component {
   play=trackUpload=>{
     this.props.playURL(trackUpload);
   }

   like=id=>{
     const track = this.props.tracks.find(tr => tr.id===id);
     console.log(track);
     fetch('http://localhost:8080/track/tracks/1/like',{
       method:'POST',
       headers: {
                'Content-Type': 'application/json',
            },
       body:JSON.stringify({
                  id:track.id,
                 trackName: track.trackName,
                 trackUpload: track.trackUpload
               })
     })

   }
    render() {
      const tracks = this.props.tracks;

        return (

            <div className="gallery">
              {this.props.tracks.map((track,index)=>
                <p key={index}>{track.trackName}
                  <button onClick={()=>this.play(track.trackUpload)}>play</button>
                  <button onClick={()=>this.like(track.id)}>like</button>
                </p>)}
            </div>
        );
    }
}
const  mapDispatchToProps = dispatch=> {
  return{
    playURL:(trackURL)=>{
      dispatch({type:'PLAYING_TRACK',payload:trackURL});
    }
  }
}

const mapStateToProps = state =>{
    return {
      playingTrack:state.playingTrack
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Gallery);
