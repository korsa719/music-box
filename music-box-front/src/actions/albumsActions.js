import {ALBUMS_LOADED} from './types.js';

export const loadAlbums = () => dispatch => {
fetch('http://localhost:8080/albums')
    .then(res => res.json())
    .then(data => dispatch({type:ALBUMS_LOADED, payload: data}))
}
