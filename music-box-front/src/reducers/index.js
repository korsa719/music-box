import { combineReducers } from 'redux'
import albums from './albums'
import tracks from './tracks'
import isEditAlbum from './isEditAlbum'
import playingTrack from './playingTrack'
import favoritesTracks from './favoritesTracks'




export default combineReducers({
    albums,
    tracks,
    isEditAlbum,
    playingTrack,
    favoritesTracks
});
