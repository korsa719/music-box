package ua.danit.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "id")
public class Track {
  @Id
  @GeneratedValue
  private int id;
  private String trackName;
  private String trackUpload;

  @ManyToOne
  @JsonIgnore
  private Album album;

  @ManyToMany(mappedBy = "tracks")
  @JsonIgnore
  private List<User> users;

  public Track() {

  }

  public Track(String trackName,String trackUpload){
    this.trackName=trackName;
    this.trackUpload=trackUpload;
  }
  public List<User> getUsers() {
    return users;
  }

  public void setUsers(List<User> users) {
    this.users = users;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTrackName() {
    return trackName;
  }

  public void setTrackName(String trackName) {
    this.trackName = trackName;
  }

  public String getTrackUpload() {
    return trackUpload;
  }

  public void setTrackUpload(String trackUpload) {
    this.trackUpload = trackUpload;
  }

  public Album getAlbum() {
    return album;
  }

  public void setAlbum(Album album) {
    this.album = album;
  }

  @Override
  public String toString() {
    return "Track{" +
        "id=" + id +
        ", trackName='" + trackName + '\'' +
        ", trackUpload='" + trackUpload + '\'' +
        ", album=" + album +
        ", users=" + users +
        '}';
  }
}
