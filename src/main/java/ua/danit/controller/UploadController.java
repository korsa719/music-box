package ua.danit.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import ua.danit.model.Track;
import ua.danit.service.StorageService;


@RestController
public class UploadController {

  @Autowired
  StorageService storageService;


  @PostMapping("/post")
  @CrossOrigin(value = "http://localhost:3000")
  @ResponseBody
  public Track handleFileUpload(@RequestParam("file") MultipartFile file) {
    Track qq= new Track();
    String message = "";
    try {
      storageService.store(file);
      System.out.println(file.getContentType());
      message =file.getOriginalFilename();
      qq.setTrackUpload(message);
      return qq;
    } catch (Exception e) {
      return qq;
    }
  }



  @GetMapping("/files/{filename:.+}")
  @ResponseBody
  public ResponseEntity<Resource> getFile(@PathVariable String filename) {
    Resource file = storageService.loadFile(filename);
    return ResponseEntity.ok()
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
        .body(file);
  }

}
