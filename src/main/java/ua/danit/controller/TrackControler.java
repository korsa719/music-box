package ua.danit.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ua.danit.model.Track;
import ua.danit.service.AlbumService;
import ua.danit.service.TrackService;


@RestController
@RequestMapping("/track")
public class TrackControler {
  @Autowired
  private TrackService trackService;

  @Autowired
  private AlbumService albumService;

  @RequestMapping(value = "/",method = RequestMethod.POST)
  @CrossOrigin (value = "http://localhost:3000")
  void addTrackToAlbum ( @RequestBody Track track){
    albumService.saveAlbumWithNewTrack(track);
  }

  @RequestMapping(path = "/tracks/{id}", method = RequestMethod.POST)
  void addTrack(@PathVariable int id, @RequestBody Track track) {
    trackService.addTrack(id, track);
  }

  @RequestMapping (value = "/tracks/{id}/like",method = RequestMethod.POST)
  @CrossOrigin (value = "http://localhost:3000")
  void addLike(@PathVariable int id,@RequestBody Track trackId){
    System.out.println("from addLike");
    System.out.println("user_id"+id);
    System.out.println("track");
    System.out.println(trackId.toString());
    trackService.addToFavorites(id,trackId);
  }

  @RequestMapping(value = "/tracks/{id}/like",method = RequestMethod.GET)
  @ResponseBody
  @CrossOrigin (value = "http://localhost:3000")
  Iterable<Track> getTrackFavoriteByUserID (@PathVariable int id){
    return trackService.getLikedTracks(id);
  }

  @RequestMapping(value = "/tracks/{id}",method = RequestMethod.DELETE)
  @CrossOrigin (value = "http://localhost:3000")
  void deleteTrack(@PathVariable int id){
    trackService.delete(id);
  }
  @RequestMapping(value = "/tracks/{id}",method = RequestMethod.PUT)
  @CrossOrigin (value = "http://localhost:3000")
  void updateTrack(@PathVariable int id ,@RequestBody Track track){
    System.out.println("trackname" + track.getTrackName());
    System.out.println("trackupload"+track.getTrackUpload());
    Track track1 = trackService.getTrackById(id);
    if (!track.getTrackName().equals("")){
      track1.setTrackName(track.getTrackName());
    }else{
      track1.setTrackName(track1.getTrackName());
    }
    if (!track.getTrackUpload().equals("")){
      track1.setTrackUpload(track.getTrackUpload());
    }else{
      track1.setTrackUpload(track1.getTrackUpload());
    }
    trackService.update(track1);
  }


}
