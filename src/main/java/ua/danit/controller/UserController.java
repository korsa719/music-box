package ua.danit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ua.danit.dao.UserDao;
import ua.danit.model.User;
import ua.danit.service.TrackService;
import ua.danit.service.UserService;

/**
 * Created  28.03.2018.
 */
@RestController
@RequestMapping("/api")
public class UserController {
  @Autowired
  private UserService userService;

  @Autowired
  private UserDao userDao;

  @Autowired
  private TrackService trackService;


  @RequestMapping(value = "/login",method = RequestMethod.POST)
  @ResponseBody
  boolean checkLogin(@RequestBody User user){
    return userService.checkUser(user);
  }

  @RequestMapping(value = "/create",method = RequestMethod.POST)
  @ResponseBody
  void create (@RequestBody User user){
     userService.createUser(user);
  }

  @RequestMapping("/users/me")
  @ResponseBody
  User getMe(@RequestBody int id){
    return userService.getUserById(id);
  }



}
