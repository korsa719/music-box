package ua.danit.service;

import ua.danit.model.User;


public interface UserService {
  User getUserById(int id);
  void createUser( User user);
  boolean checkUser(User user);
  void update(User user);
}
