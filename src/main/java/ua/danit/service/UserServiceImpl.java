package ua.danit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.danit.dao.UserDao;
import ua.danit.model.User;


@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private UserDao userDao;

  @Override
  public User getUserById(int id) {
    return userDao.findOne(id);
  }

  @Override
  public void createUser(User user) {
    userDao.save(user);
  }

  @Override
   public boolean checkUser(User user){
    for(User us:userDao.findAll()){
      if (us.getName().equals(user.getName())&&us.getPassword().equals(user.getPassword())){
        return true;
      }
    }
    return false;
   }

  @Override
  public void update(User user) {
    userDao.save(user);

  }
}
