package ua.danit.service;


import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import ua.danit.model.Album;
import ua.danit.model.Track;


public interface AlbumService {
  Album addAlbum(@RequestBody Album album);

  Iterable<Album> getAllAlbum();

  Album getAlbumById(int id);

  void update(@PathVariable int id, @RequestBody Album album);

  void delete(@PathVariable int id);

  void deleteCover(@PathVariable int id);
  void  saveAlbumWithNewTrack(Track track);
}
