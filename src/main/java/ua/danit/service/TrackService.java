package ua.danit.service;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import ua.danit.model.Track;


public interface TrackService {

  void addTrack(@PathVariable int id, @RequestBody Track track);

  Iterable<Track> getAllTracks();

  Iterable<Track> getLikedTracks(int id);

  Track getTrackById(int id);

  void update(@RequestBody Track track);

  void delete(@PathVariable int id);

  void addToFavorites(@PathVariable int id,@RequestBody Track track);

}
