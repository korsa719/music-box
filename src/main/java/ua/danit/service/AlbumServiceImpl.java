package ua.danit.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.danit.dao.AlbumDao;
import ua.danit.model.Album;
import ua.danit.model.Track;


@Service
public class AlbumServiceImpl implements AlbumService {

  @Autowired
  private AlbumDao albumDao;

  @Override
  public Album addAlbum(Album album) {
    return albumDao.save(album);
  }

  @Override
  public Iterable<Album> getAllAlbum() {
    return albumDao.findAll();
  }

  @Override
  public Album getAlbumById(int id) {
    return albumDao.findOne(id);
  }

  @Override
  public void update(int id, Album album) {
    Album currentAlbum = albumDao.findOne(id);
    currentAlbum.setCover(album.getCover());
    currentAlbum.setTitle(album.getTitle());
    albumDao.save(currentAlbum);
  }

  @Override
  public void delete(int id) {
    albumDao.delete(id);
  }

  @Override
  public void deleteCover(int id) {
    Album newAlbum = albumDao.findOne(id);
    newAlbum.setCover(null);
    albumDao.save(newAlbum);

  }

  @Override
  public void saveAlbumWithNewTrack(Track track) {
    Album album = albumDao.findOne(track.getId());
    Track newTrack = new Track();
    newTrack.setTrackName(track.getTrackName());
    newTrack.setTrackUpload(track.getTrackUpload());
    List<Track> tracks = album.getTracks();
    tracks.add(newTrack);
    album.setTracks(tracks);
    albumDao.save(album);
  }
}
