package ua.danit.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import ua.danit.model.User;

public interface UserDao extends JpaRepository<User, Integer> {


}
