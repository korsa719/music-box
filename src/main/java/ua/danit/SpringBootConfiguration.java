package ua.danit;

import javax.annotation.Resource;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ua.danit.service.StorageService;


@SpringBootApplication
public class SpringBootConfiguration implements CommandLineRunner {
  @Resource
  StorageService storageService;

  public static void main(String[] args) {
    SpringApplication.run(SpringBootConfiguration.class);
  }
  @Override
  public void run(String... args) throws Exception {

    storageService.init();
  }
}
